"""
VENTANA LOGIN DE FARMACIA
"""
import MySQLdb
import os
import sys
import re
import hashlib
#--------------------------------------
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtMultimedia import *
#--------------------------------------
import sistema
#--------------------------------------


class Ui_Term(QDialog):
    def __init__(self, args, parent=None):
        QDialog.__init__(self, parent)

        self.setWindowTitle("Sistema de Surtido")
        self.setWindowIcon(QIcon("icon/Pumps-48.png"))
        self.setFixedSize(300, 300)
        self.move(QDesktopWidget().availableGeometry().center() - self.frameGeometry().center())

        labelFondo = QLabel(self)
        labelFondo.setPixmap(QPixmap("icon/wallpaper.jpg"))
        labelFondo.setGeometry(0, 0, 300, 300)
        db = MySQLdb.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
        query = db.cursor()

        codigoid = args[0]
        query.execute("SELECT CodigoBarra FROM Almacen WHERE ID="+codigoid+"")
        for row in query:
            self.idcode = str(row[0])

        self.botones()

#----------------------------------------------------------------------------

    def botones(self):
        grop = QGroupBox("Cantidades:", self)
        grop.setGeometry(10, 40, 280, 80)

        label1 = QLabel("Codigo de Barra:", self)
        label1.move(100, 10)
        self.label1 = QLabel("777779999997",self)
        self.label1.setGeometry(89,15,120,40)
        self.label1.setText(self.idcode)
        self.label1.setAlignment(Qt.AlignCenter)
        self.label1.setStyleSheet("font-size: 15px;color: rgb(0,0,0)")
        

        label2 = QLabel("Unidad:", self)
        label2.move(13, 70)
        self.line2 = QLineEdit(self)
        self.line2.setPlaceholderText("0")
        self.line2.setAlignment(Qt.AlignCenter)
        self.line2.setGeometry(15, 85, 40, 20)
    

        label3 = QLabel("Tableta:", self)
        label3.move(71, 70)
        self.line3 = QLineEdit(self)
        self.line3.setPlaceholderText("0")
        self.line3.setAlignment(Qt.AlignCenter)
        self.line3.setGeometry(75, 85, 40, 20)
      

        label4 = QLabel("Caja:", self)
        label4.move(138, 70)
        self.line4 = QLineEdit(self)
        self.line4.setPlaceholderText("0")
        self.line4.setAlignment(Qt.AlignCenter)
        self.line4.setGeometry(135, 85, 40, 20)
    

        label5 = QLabel("Bolsa:", self)
        label5.move(192, 70)
        self.line5 = QLineEdit(self)
        self.line5.setPlaceholderText("0")
        self.line5.setAlignment(Qt.AlignCenter)
        self.line5.setGeometry(190, 85, 40, 20)
        

        label55 = QLabel("Frasco:", self)
        label55.move(245, 70)
        self.line55 = QLineEdit(self)
        self.line55.setPlaceholderText("0")
        self.line55.setAlignment(Qt.AlignCenter)
        self.line55.setGeometry(245, 85, 40, 20)

#----------------------------------------------------------------------------

        grop = QGroupBox("Presios:", self)
        grop.setGeometry(10, 120, 280, 80)


        label6 = QLabel("Unidad:", self)
        label6.move(13, 150)
        self.line6 = QLineEdit(self)
        self.line6.setPlaceholderText("$ 0.0")
        self.line6.setAlignment(Qt.AlignCenter)
        self.line6.setGeometry(15, 165, 45, 20)
    

        label7 = QLabel("Tableta:", self)
        label7.move(71, 150)
        self.line7 = QLineEdit(self)
        self.line7.setPlaceholderText("$ 0.0")
        self.line7.setAlignment(Qt.AlignCenter)
        self.line7.setGeometry(73, 165, 45, 20)
      

        label8 = QLabel("Caja:", self)
        label8.move(138, 150)
        self.line8 = QLineEdit(self)
        self.line8.setPlaceholderText("$ 0.0")
        self.line8.setAlignment(Qt.AlignCenter)
        self.line8.setGeometry(133, 165, 45, 20)
    

        label9 = QLabel("Bolsa:", self)
        label9.move(192, 150)
        self.line9 = QLineEdit(self)
        self.line9.setPlaceholderText("$ 0.0")
        self.line9.setAlignment(Qt.AlignCenter)
        self.line9.setGeometry(187, 165, 45, 20)
        

        label10 = QLabel("Frasco:", self)
        label10.move(242, 150)
        self.line10 = QLineEdit(self)
        self.line10.setPlaceholderText("$ 0.0")
        self.line10.setAlignment(Qt.AlignCenter)
        self.line10.setGeometry(241, 165, 45, 20)


        self.botTerm = QPushButton(self)
        self.botTerm.setGeometry(120, 220, 48, 48)
        self.botTerm.setIcon(QIcon(QPixmap("icon/Up-48.png")))
        self.botTerm.setIconSize(QSize(48, 48))
        self.botTerm.setStyleSheet("background: rgb(255, 255, 255, 0);")
        self.botTerm.setFlat(True)
        self.botTerm.clicked.connect(self.surtirDatos)

    def surtirDatos(self):
        listado = [self.line2.text(), 
        self.line3.text(), self.line4.text(), 
        self.line5.text(), self.line55.text(),
        self.line6.text(), self.line7.text(),
        self.line8.text(), self.line9.text(),
        self.line10.text()]


        if listado[0] == "":
            listado[0] = "0"
        else:
            listado[0]

        if listado[1] == "":
            listado[1] = "0"
        else:
            listado[1]

        if listado[2] == "":
            listado[2] = "0"
        else:
            listado[2]

        if listado[3] == "":
            listado[3] = "0"
        else:
            listado[3]

        if listado[4] == "":
            listado[4] = "0"
        else:
            listado[4]

        if listado[5] == "":
            listado[5] = "0"
        else:
            listado[5]

        if listado[6] == "":
            listado[6] = "0"
        else:
            listado[6]

        if listado[7] == "":
            listado[7] = "0"
        else:
            listado[7]

        if listado[8] == "":
            listado[8] = "0"
        else:
            listado[8]

        if listado[9] == "":
            listado[9] = "0"
        else:
            listado[9]



        try:
            db = MySQLdb.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
            query = db.cursor()
            query.execute("UPDATE Almacen SET Unidades = "+listado[0]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Almacen SET Tabletas = "+listado[1]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Almacen SET Cajas = "+listado[2]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Almacen SET Bolsas = "+listado[3]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Almacen SET Frascos = "+listado[4]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()

            query.execute("UPDATE Almacen SET Presio_Cliente = "+listado[5]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Almacen SET Presio_Unidad = "+listado[5]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Almacen SET Presio_Tableta = "+listado[6]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Almacen SET Presio_Caja = "+listado[7]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Almacen SET Presio_Bolsa = "+listado[8]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Almacen SET Presio_Botella = "+listado[9]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()

            query.execute("UPDATE Info SET Unidades = "+listado[0]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Info SET Tabletas = "+listado[1]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Info SET Cajas = "+listado[2]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Info SET Bolsas = "+listado[3]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            query.execute("UPDATE Info SET Frascos = "+listado[4]+" WHERE CodigoBarra = '" + self.label1.text() + "'")
            db.commit()
            QSound.play("song/insertar.wav")
            QMessageBox.information(self, 'Informacion', 'Informacion exitosa', QMessageBox.Ok)
            self.close()
        except:
            QMessageBox.warning(self, 'Warning',
                                    'Error con la al conectar con la DB.', QMessageBox.Ok)
        
#----------------------------------------------------------------------------
if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = Ui_Term()
    ex.show()
    sys.exit(app.exec_())
