import sys
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt

import os
import sys
import re
import hashlib
import pandas
import MySQLdb
import random

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class Window(QDialog):
    def __init__(self, args, parent=None):
        super(Window, self).__init__(parent)
        self.setWindowTitle("Cuadro bar")
        self.setWindowIcon(QIcon("icon/Pumps-48.png"))
        self.setFixedSize(600, 400)

        self.fecha1 = args[0]
        self.fecha2 = args[1]

        # a figure instance to plot on
        self.figure = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.plot()

        # set the layout
        layout = QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)

        self.setLayout(layout)

    def plot(self):
        db2 = MySQLdb.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
        query2 = db2.cursor()
        if (query2.execute("SELECT SUM(Total) AS suma FROM Recibo where Fecha BETWEEN '" + self.fecha1 + "' AND '" + self.fecha2 + "'")):
            for row2 in query2:
                dinero = int(row2[0])

        db = MySQLdb.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
        query = db.cursor()
        if (query.execute("SELECT Fecha, Total FROM Recibo where Fecha BETWEEN '" + self.fecha1 + "' AND '" + self.fecha2 + "' ORDER BY Fecha ASC")):

            for r, row in enumerate(query, start=1):
                x = [int(row[1])]
                plt.axes((0.1, 0.3, 0.8, 0.6))
                plt.bar(r, x)
                plt.ylim(50, dinero)
                plt.title('Sistema de ventas')
                plt.ylabel('Dinero')
            self.canvas.draw()

if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())