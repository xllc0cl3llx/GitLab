# -*- coding: utf-8 -*-

import sys
import os
import re
import hashlib
import MySQLdb

from Login import *
from sistema import *




class Main_Login(QtWidgets.QDialog):
	def __init__(self, parent=None):
		QtWidgets.QDialog. __init__(self, parent)
		self.ui = Ui_Login()
		self.ui.setupUi(self)
		self.setFixedSize(240, 320)
		self.setWindowTitle("Login")

		self.ui.lineEdit.textChanged.connect(self.validateUsarname)
		self.ui.lineEdit_2.textChanged.connect(self.validatePassword)
		self.ui.pushButton.clicked.connect(self.validateStart)

#----------------------------------------------------------------------------
	def validateUsarname(self):
		validar = re.match('^[a-z[0-9\sáéíóúàèìòùäëïöüñ]+$', self.ui.lineEdit.text(), re.I)
		if self.ui.lineEdit.text() == "":
			self.ui.lineEdit.setStyleSheet("border: 2px solid yellow;")
			return False
		elif not validar:
			self.ui.lineEdit.setStyleSheet("border: 2px solid red;")
			return False
		else:
			self.ui.lineEdit.setStyleSheet("border: 3px solid cyan;")
			return True
#----------------------------------------------------------------------------
	def validatePassword(self):
		if self.ui.lineEdit_2.text() == "":
			self.ui.lineEdit_2.setStyleSheet("border: 3px solid yellow;")
			return False
		else:
			self.ui.lineEdit_2.setStyleSheet("border: 3px solid cyan;")
			return True
#----------------------------------------------------------------------------
	def validateStart(self):
		QtMultimedia.QSound.play("song/click.wav")
		if self.validateUsarname() and self.validatePassword():
			usuario = hashlib.sha512(self.ui.lineEdit.text().encode('utf-8')).hexdigest()
			password = hashlib.sha512(self.ui.lineEdit_2.text().encode('utf-8')).hexdigest()
			try:
				db = MySQLdb.connect(host="localhost", user="root", passwd="Lb104572", db="admin")
				query = db.cursor()
				if (query.execute("SELECT * FROM Registros WHERE Usuario='" + usuario + "' AND Clave='" + password + "'")):
					db.commit()
					valido = QtWidgets.QMessageBox.information(self, 'Informacion', 'Bienvenido ' + self.ui.lineEdit.text(), QtWidgets.QMessageBox.Ok)
					if valido == QtWidgets.QMessageBox.Ok:
						self.Ven2()
					else:
						pass
				else:
					QtWidgets.QMessageBox.warning(self, 'Alerta', 'Usuario y Clave son invalido o no existen.', QtWidgets.QMessageBox.Ok)
					self.ui.lineEdit.clear()
					self.ui.lineEdit_2.clear()
					self.ui.lineEdit.setStyleSheet("border: 2px solid red;")
					self.ui.lineEdit_2.setStyleSheet("border: 2px solid red;")
			except:
				QtWidgets.QMessageBox.critical(self, 'Fallo', 'Error en la conexion SQL.', QtWidgets.QMessageBox.Ok)
				self.ui.lineEdit.clear()
				self.ui.lineEdit_2.clear()

#----------------------------------------------------------------------------
	def Ven2(self):
		self.close()
		v2 = Ventana(self)
		v2.show()
		
if __name__=="__main__":
	app = QtWidgets.QApplication(sys.argv)
	myapp = Main_Login()
	myapp.show()
	sys.exit(app.exec_())
