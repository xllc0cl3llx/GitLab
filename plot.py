import sys
from PyQt5.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt

import os
import sys
import re
import hashlib
import pandas
import MySQLdb
import random

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class Window(QDialog):
    def __init__(self, args, parent=None):
        super(Window, self).__init__(parent)
        self.setWindowTitle("Cuadro plot")
        self.setWindowIcon(QIcon("icon/Pumps-48.png"))
        self.setFixedSize(600, 400)
        self.fecha1 = args[0]
        self.fecha2 = args[1]

        # a figure instance to plot on
        self.figure = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.plot()

        # set the layout
        layout = QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)

        self.setLayout(layout)

    def plot(self):
        ''' plot some random stuff '''
        # random data
        db = MySQLdb.connect(host="localhost", user="root", passwd="Lb104572", db="inventario")
        query = db.cursor()
        query = "SELECT Fecha, Total FROM Recibo where Fecha BETWEEN '"+self.fecha1+"' AND '"+self.fecha2+"' ORDER BY Fecha ASC"
        df = pandas.read_sql(query, db, index_col=['Fecha'])
        fig, ax = plt.subplots()
        self.figure.clear()
        ax = self.figure.add_subplot(111)
        df.plot(ax=ax)
        self.canvas.draw()

if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())